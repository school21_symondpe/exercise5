| | | | |
|-|-|-|-|
|Задание №3. Веб-хранилища №2| | | |
| | | | |
| | | | |
| | | | |
|Объекты из "Local Storage"| | | |
|Сайт|Ключ|Значение|Описание|
|https://m.dzen.ru/pogoda/44|__WEATHER_LOCALIZATION_LANGUAGE_KEY__|ru|локализация по РФ|
|https://www.odin.study/|_ym85243030_il|"ДQA 10310"|выбранный раздел|
|https://www.softwaretestinghelp.com/|btUserCountry|RU| Страна пользователя|
|https://app-live.browserstack.com/|last_session_source_info|{"selectedSource":"app-store"}| последний выбранный источник |
|https://team-arpd.testit.software/|test-plan-sidebar|{"isClosed":true}| статус сайдбара План|
|https://m.dzen.ru/pogoda/44|_ym89808742_il| "Среда, сегодняднём+24°2,9&nbsp;м/с, СВвечером+23°2,9&nbsp;м/с, СВночью+16°1,7&nbsp;м/с, СВ752мм рт. "| погода по выбранному региону|
|Сайт|Ключ|Значение|Описание|
|Объекты из "Session Storage"| | | |
|https://m.dzen.ru/pogoda/| sessionId|9.96394E+18| id сессии|
|https://dev.azure.com/|msameid| 32743037-42e9-4a66-4882-d6db7f5644fc| id сессии|
|https://sbermegamarket.ru/landing/shopping-fest/| url-history.current| {"fullUrl":"https://sbermegamarket.ru/landing/shopping-fest/"}| текущий url|
|https://sbermegamarket.ru/| url-history.prev| {"fullUrl":"https://sbermegamarket.ru/landing/shopping-fest/"}| предыдущий url|
|https://www.youtube.com/| yt-remote-session-name| {"data":"Desktop","creation":1686724865052}| название сессии и вид|


## Добавление объектов 

![comand1.png](src/comand1.png)

![result1.png](src/result1.png)

![comand2.png](src/comand2.png)

![result2.png](src/result2.png)
